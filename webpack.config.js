const path =  require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = (env) =>{

    const plugins = [
        new HtmlWebpackPlugin({
            template: path.join(__dirname,'public',env.htmlName)
        })
    ]


    if(env.NODE_ENV === 'production'){
        console.log('Nada de configs Produccion');
    }else{
        plugins.push(
            new MiniCssExtractPlugin({
                filename: '[name].css'
            })
        )
    }

    return{
        entry:'./public/index.js',
        output: path.resolve(__dirname,'/public'),
        
        resolve:{
            extensions:['.js']
        },

        module:{
            rules:[
                {
                    test:/\.(js)$/,
                    exclude:'/node_modules/',
                    use:{
                        loader: 'babel-loader'
                    }
                },
                {
                    test:/\.html$/,
                    use:{
                        loader: 'html-loader'
                    }
                },
                {
                    test: /\.css$/,
                    use:[MiniCssExtractPlugin.loader, 'css-loader']
                },
                {
                    test: /\.(png|jpg|gif|svg)$/i,
                    use: [
                      {
                        loader: 'url-loader',
                        options: {
                          limit: 8192,
                        },
                      },
                    ],
                },
    
            ]
        },
        plugins,
        optimization: {
            splitChunks: {
                cacheGroups: {
                        commons: {
                            test: /[\\/]node_modules[\\/]/,
                            name: "vendor",
                            chunks: "initial",
                        },
                    },
                },
              runtimeChunk: {
                name: "manifest",
              },
        },
        devServer:{
            open: true
        }
    

    }

}